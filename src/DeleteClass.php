<?php

class DeleteClass
{
    public $id;
    public $pdo;

    public function __construct()
    {
        $this->pdo = new PdoClass;
    }

    public function delete($id)
    {
        $this->id = (int)$id;
        $this->pdo->deleteContact($this->id);
    }
}