<?php

class indexClass
{
    public $pdo;

    public function __construct()
    {
        $this->pdo = new PdoClass();
    }


    public function printResults() {
        // Attempt select query execution
        $rows = $this->pdo->getAllContacts();

        if(count($rows) > 0){
            echo '<table class="table table-bordered table-striped">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>#</th>";
            echo "<th>Nom</th>";
            echo "<th>Prenom</th>";
            echo "<th>Action</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            foreach($rows as $row){
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nom'] . "</td>";
                echo "<td>" . $row['prenom'] . "</td>";
                echo "<td>";
                echo '<a href="update.php?id='. $row['id'] . '" class="mr-3" title="Modifier" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
                echo '<a href="delete.php?id='. $row['id'] .'" title="Supprimer" data-toggle="tooltip"><span class="fa fa-trash"></span></a>';
                echo "</td>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
            // Free result set

        } else{
            echo "Erreur, la BDD n'a pas été contactée";
        }

    }
}