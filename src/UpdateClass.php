<?php
declare(strict_types=1);

class UpdateClass
{
    public $nom;
    public $id;
    public $prenom;
    public $pdo;

    public function __construct($nom, $prenom, $id)
    {
        $this->pdo = new PdoClass();
        $this->nom = $nom;
        $this->id = $id;
        $this->prenom = $prenom;
    }

    public function checkInput() {

        if($_SERVER["REQUEST_METHOD"] == "POST") {
            // Validate nom
            $input_nom = trim($_POST["Nom"]);
            if (empty($input_nom)) {
                echo "Please enter a nom. <br> ";
            } elseif (!filter_var($input_nom, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z\s]+$/")))) {
                echo "Please enter a valid nom.";
            } else {
                $this->nom = $input_nom;
            }

            // Validate prenom
            $input_prenom = trim($_POST["Prenom"]);
            if (empty($input_prenom)) {
                echo "Please enter an prenom.";
            } else {
                $this->prenom = $input_prenom;
            }
        }
    }

    public function updateUser() {

            $success = $this->pdo->updateContact($this->nom, $this->prenom, $this->id);

            // Attempt to execute the prepared statement
            if($success){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
    }

}