<?php

require_once "config.php";
include "ResearchClass.php";
include "PdoClass.php";


// Process delete operation after confirmation
if(isset($_POST["Nom"]) && !empty($_POST["Nom"]) && isset($_POST["Prenom"]) && !empty($_POST["Prenom"])){
    $researchClass = new ResearchClass($_POST["Prenom"], $_POST["Nom"]);

    $researchClass->research();

    // Prepare a delete statement
} else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Research Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">

    </div>
</div>
</body>
</html>