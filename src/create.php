<?php
// Include config file
require_once "config.php";
include "CreateClass.php";
include "PdoClass.php";




$createClass = new CreateClass();
$input_nom = trim($_POST["nom"]);
$input_prenom = trim($_POST["prenom"]);

$createClass->checkInput($input_nom, $input_prenom);
$nom = $prenom = "";
$nom_err = $prenom_err ="";

if(isset($createClass->nom) && $createClass->prenom) {
    $createClass->addUser();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mt-5">Créer contact</h2>
                <p>Remplissez le formulaire pour ajouter un contact dans la database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                        <label>Nom</label>
                        <input type="text" name="nom" class="form-control <?php echo (!empty($nom_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $nom; ?>">
                        <span class="invalid-feedbnack"><?php echo $nom_err;?></span>
                    </div>
                    <div class="form-group">
                        <label>Prenom</label>
                        <input name="prenom" class="form-control <?php echo (!empty($prenom_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $prenom; ?>">
                        <span class="invalid-feedback"><?php echo $prenom_err;?></span>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Envoyer">
                    <a href="index.php" class="btn btn-secondary ml-2">Annuler</a>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
