<?php
// Include config file
require_once "config.php";
include "UpdateClass.php";
include "PdoClass.php";


// Define variables and initialize with empty values
$nom = $prenom = "";
$nom_err = $prenom_err = "";
var_dump($_POST);
$id = $_GET['id'];
if (isset($_POST['Prenom'])) {
    echo 'oui';
    $updateClass = new UpdateClass($_POST["Nom"], $_POST["Prenom"], $_POST["id"]);
    if(isset($updateClass->id)) {
         $updateClass->checkInput();
        echo 'oui';
    }
    if(isset($updateClass->nom) && $updateClass->prenom) {
         $updateClass->updateUser();
        header("location: index.php");
        exit();

    } else {
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mt-5">Update Record</h2>
                <p>Please edit the input values and submit to update the employee record.</p>
                <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                    <div class="form-group">
                        <label>nom</label>
                        <input type="text" name="Nom" class="form-control <?php echo (!empty($nom_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $nom; ?>">
                        <span class="invalid-feedback"><?php echo $nom_err;?></span>
                    </div>
                    <div class="form-group">
                        <label>Prenom</label>
                        <textarea name="Prenom" class="form-control <?php echo (!empty($prenom_err)) ? 'is-invalid' : ''; ?>"><?php echo $prenom; ?></textarea>
                        <span class="invalid-feedback"><?php echo $prenom_err;?></span>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>