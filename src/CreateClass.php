<?php
declare(strict_types=1);

class CreateClass
{
    public $nom;
    public $prenom;
    public $pdo;

    public function __construct()
    {
        $this->pdo = new PdoClass();
        $this->nom = "";
        $this->prenom = "";
    }


    public function checkInput($input_nom, $input_prenom) {

            if (empty($input_nom)) {
                throw new InvalidArgumentException(
                    sprintf(
                        '"%s" is not a valid email address',
                        $input_nom
                    )
                );            } elseif (!filter_var($input_nom, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z\s]+$/")))) {
                throw new InvalidArgumentException(
                    sprintf(
                        '"%s" is not a valid email address',
                        $input_nom
                    )
                );
            } else {
                $this->nom = $input_nom;
            }

            // Validate prenom
            if (empty($input_prenom)) {
                throw new InvalidArgumentException(
                    sprintf(
                        '"%s" is not a valid email address',
                        $input_prenom
                    )
                );            } else {
                $this->prenom = $input_prenom;
            }
            return true;
        }

    public function addUser() {

            $success = $this->pdo->createContact($this->nom, $this->prenom);

            // Attempt to execute the prepared statement
            if($success){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
    }

}