<?php

class ResearchClass
{
    public String $nom;
    public String $prenom;
    public PdoClass $pdo;

    public function __construct($nom, $prenom)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->pdo = new PdoClass();
    }

    public function research() {

        $rows = $this->pdo->researchContact($this->prenom, $this->nom);

        if(count($rows) > 0){
            echo '<table class="table table-bordered table-striped">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>#</th>";
            echo "<th>Nom</th>";
            echo "<th>Prenom</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            foreach($rows as $row){
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nom'] . "</td>";
                echo "<td>" . $row['prenom'] . "</td>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
            // Free result set

        } else{
            echo "Erreur, la BDD n'a pas été contactée";
        }

    }
}