<?php

class PdoClass
{
    public $pdo;

    public function __construct()
    {

        $pdo = new PDO('sqlite:'.'../database.sqlite');

        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->pdo = $pdo;
    }

    function getAllContacts()
    {
        $req = $this->pdo->query("SELECT * from contacts");
        $row = $req->fetchAll();
        // si req ok (!false)
        if ($req)
        {
            return $row;
        }
    }
    function createContact(string $nom, string $prenom): bool
    {
        $stmt = $this->pdo->prepare("INSERT INTO contacts (nom, prenom) VALUES (:nom, :prenom)");
        $result = $stmt->execute(array(
            'nom'         => $nom,
            'prenom'       => $prenom
        ));

        return $result;
    }

    function updateContact($nom, $prenom, $id)
    {
        $nom = "'" . $nom . "'";
        $prenom = "'" . $prenom . "'";

        $stmt = $this->pdo->prepare("UPDATE contacts SET nom=". $nom .", prenom=".$prenom." WHERE id=" . $id);
        $result = $stmt->execute();

        return $result;
    }
    function deleteContact(int $id)
    {

        $stmt = $this->pdo->query('DELETE FROM contacts WHERE id =' . $id);
        $result = $stmt->execute();

        return $result;

    }
    function researchContact(String $nom, String $prenom)
    {
    $nom = "'" . $nom . "'";
    $prenom = "'" . $prenom . "'";

    $stmt = $this->pdo->query('SELECT * FROM contacts WHERE nom = ' .$nom. ' AND prenom = ' .$prenom);
    $result = $stmt->fetchAll();

    return $result;

    }

}