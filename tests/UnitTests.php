<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
include "../src/PdoClass.php";
include "../src/CreateClass.php";

final class UnitTests extends TestCase
{
    public function testCanBeCreatedFromValidDatas(): void
    {
        $pdoClass = new CreateClass();
        $this->assertTrue($pdoClass->checkInput('Rousseaud', 'Simon'));
    }
    public function testCantBeCreatedFromInvalidDatas(): void
    {
        $pdoClass = new CreateClass();
        $this->expectException(InvalidArgumentException::class);
        $this->assertTrue($pdoClass->checkInput(12345, 'Simon'));
    }


}
